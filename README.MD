# TSA REST service

### Running locally
For development, you can run the service using Flask:

`export FLASK_APP=pytsa_service/__init__.py`
`FLASK_DEBUG=1 flask run`

Local URL's:
http://localhost:5000/tsa/wps?service=WPS&request=GetCapabilities

http://localhost:5000/tsa/processes

Execute is done by posting a process graph to:
http://localhost:5000/tsa/execute