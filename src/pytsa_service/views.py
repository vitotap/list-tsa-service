import os
import logging

from flask import request, url_for, jsonify, send_from_directory,json

from pytsa_service import app
from .ProcessGraphDeserializer import graphToRdd, health_check
import pandas as pd
from pywps import Service
from pytsa_service.processes.timeseries import TimeSeriesProcess
from pytsa_service.processes.smooth import SmoothProcess
from pytsa_service.processes.EMWA import EMWAProcess
from pytsa_service.processes.peak_signals import PeakSignalsProcess
from pytsa_service.processes.seasonal_decomposition import SeasonalDecompositionProcess
from pytsa_service.processes.mann_kendall import MannKendallProcess
from pytsa_service.processes.detrender import DetrenderProcess
from pytsa_service.processes.savitzky_golay import SavitzkyGolayProcess
from pytsa_service.processes.arima_model import ArimaModelProcess

ROOT = '/tsa'

processes = [
    TimeSeriesProcess(),
    SmoothProcess(),
    EMWAProcess(),
    PeakSignalsProcess(),
    SeasonalDecompositionProcess(),
    MannKendallProcess(),
    DetrenderProcess(),
    ArimaModelProcess(),
    SavitzkyGolayProcess()]

service = Service(processes, ['pywps.cfg'])

@app.errorhandler(Exception)
def handle_invalid_usage(error):
    logging.exception(error)

    error_json = {
        "message":str(error)
    }
    response = jsonify(error_json)
    response.status_code = 500
    return response

@app.route('%s' % ROOT)
def index():
    return 'TSA backend. <p/>'\
           + url_for('execute') \
           + '<br/>Processes: ' + url_for('get_processes')

@app.route('%s/execute' % ROOT, methods=['GET', 'POST'])
def execute():
    if request.method == 'POST':
        print("Handling request: "+str(request))
        print("Post data: "+str(request.data))

        post_data = request.get_json()
        timeseries = graphToRdd(post_data['process_graph'], None)
        return jsonify(timeseries)
    else:
        return 'Usage: Directly evaluate process graph using POST.'


@app.route('%s/wps' % ROOT, methods=['GET', 'POST'])
def wps():
    return service


@app.route('%s/processes' % ROOT, methods=['GET'])
def get_processes():
    def args(process):
        return {input.identifier: {"description": input.title} for input in process.inputs}

    summary = [{'process_id': process.identifier, 'description': process.title, 'args': args(process)} for process in processes]
    return jsonify(summary)
