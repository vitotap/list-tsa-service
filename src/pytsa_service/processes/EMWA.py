from pywps import Process, ComplexInput, Format, ComplexOutput


class EMWAProcess(Process):
    def __init__(self):
        inputs = [ComplexInput('timeseries', "input time series", [Format('JSON')])]
        outputs = [ComplexOutput('timeseries', "output time series", [Format('JSON')])]

        super().__init__(
            self._handler,
            'EMWA',
            "EMWA-smooths a time series",
            inputs=inputs,
            outputs=outputs
        )

    def _handler(self, request, response):
        pass
