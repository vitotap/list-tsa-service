from pywps import Process, ComplexInput, Format, ComplexOutput, LiteralInput
from pytsa_service.ProcessGraphDeserializer import getTimeSeries
from flask import jsonify
import json


class TimeSeriesProcess(Process):
    def __init__(self):
        inputs = [LiteralInput('product_id', "product ID", 'string'),
                  ComplexInput('feature', "feature", [Format('GEOJSON')])]

        outputs = [ComplexOutput('timeseries', "output time series", [Format('JSON')])]

        super().__init__(
            self._handler,
            'timeseries',
            "Calculates a time series",
            inputs=inputs,
            outputs=outputs
        )

    def _handler(self, request, response):
        product_id = request.inputs['product_id'][0].data

        feature = json.loads(request.inputs['feature'][0].data)
        feature["geometry_json"] = feature["geometry"]

        if "name" not in feature:
            feature["name"] = feature.get("id", "Unknown")

        time_series = getTimeSeries(product_id, feature)

        response.outputs['timeseries'].data = jsonify(time_series).get_data(as_text=True)
        return response
