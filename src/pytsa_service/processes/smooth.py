from pywps import Process, ComplexInput, Format, ComplexOutput


class SmoothProcess(Process):
    def __init__(self):
        inputs = [ComplexInput('timeseries', "input time series", [Format('JSON')])]
        outputs = [ComplexOutput('timeseries', "output time series", [Format('JSON')])]

        super().__init__(
            self._handler,
            'smooth',
            "Smooths a time series",
            inputs=inputs,
            outputs=outputs
        )

    def _handler(self, request, response):
        input_time_series = request.inputs['timeseries'][0].data

        # TODO: actually smooth it
        smoothed_time_series = input_time_series

        response.outputs['timeseries'].data = smoothed_time_series
        return response
