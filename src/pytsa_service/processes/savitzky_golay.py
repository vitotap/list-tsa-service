from pywps import Process, ComplexInput, Format, ComplexOutput


class SavitzkyGolayProcess(Process):
    def __init__(self):
        inputs = [ComplexInput('timeseries', "input time series", [Format('JSON')])]
        outputs = [ComplexOutput('timeseries', "output time series", [Format('JSON')])]

        super().__init__(
            self._handler,
            'savitzky_golay',
            "Apply Savitzky-Golay smoothing to a time series",
            inputs=inputs,
            outputs=outputs
        )

    def _handler(self, request, response):
        pass
