from pywps import Process, ComplexInput, Format, ComplexOutput


class MannKendallProcess(Process):
    def __init__(self):
        inputs = [ComplexInput('timeseries', "input time series", [Format('JSON')])]
        outputs = [ComplexOutput('timeseries', "output time series", [Format('JSON')])]

        super().__init__(
            self._handler,
            'mann_kendall',
            "Mann-Kendall test for monotonic trend",
            inputs=inputs,
            outputs=outputs
        )

    def _handler(self, request, response):
        pass
