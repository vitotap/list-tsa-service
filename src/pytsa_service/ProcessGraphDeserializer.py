import base64
import importlib
import json
import os
import pickle
from typing import Dict, List
from dataclient.tsservice  import get_timeseries
import pandas as pd


def getTimeSeries(product_id:str, features) -> pd.Series:
    curve = get_timeseries(features,product_id, None, None)
    curve = curve.dropna()
    return {
        "timeseries": curve,
        "product_id":product_id
    }


def health_check():
    return "Default health check OK!"



def graphToRdd(processGraph:Dict, viewingParameters):
    if 'product_id' in processGraph:
        feature_ = processGraph['feature']
        feature_["geometry_json"] = feature_["geometry"]
        if "name" not in feature_:
            feature_["name"] = feature_.get("id","Unknown")
        return getTimeSeries(processGraph['product_id'], feature_)
    elif 'process_graph' in processGraph:
        return graphToRdd(processGraph['process_graph'],viewingParameters)
    elif 'process_id' in processGraph:
        return getProcessImageCollection(processGraph['process_id'],processGraph['args'],viewingParameters)
    else:
        raise AttributeError("Process should contain either collection_id or process_id, but got: \n" + json.dumps(processGraph,indent=1))


def extract_arg(args:Dict,name:str)->str:
    try:
        return args[name]
    except KeyError:
        raise AttributeError(
            "Required argument " +name +" should not be null in band_arithmetic. Arguments: \n" + json.dumps(args,indent=1))


def smooth(input_collection:List[pd.Series], args:Dict)->pd.DataFrame:
    from pytsa.trend import temporal_smoothing_scipy
    series = temporal_smoothing_scipy(get_single_input(input_collection).dropna())

    return {
        "timeseries": series,
        "process_id":"smooth",
        "sources":input_collection
    }


def get_single_input(input_collection):
    return input_collection[0]['timeseries']


def mann_kendall(input_collection:List[pd.Series], args:Dict):
    from pytsa.trend import mann_kendall
    trend, h, p, z = mann_kendall(get_single_input(input_collection))
    return {
        "timeseries":input_collection,
        "sources":input_collection,
        "process_id":"mann_kendall",
        "trend":trend,
        "h":h,
        "p":p,
        "z":z
    }

def detrender(input_collection:List[pd.Series], args:Dict) -> pd.DataFrame:
    from pytsa.trend import de_trender
    return {
        "timeseries":de_trender(get_single_input(input_collection)),
        "sources":input_collection,
        "process_id":"detrender",
    }

def EMWA(input_collection:List[pd.Series], args:Dict) -> Dict:
    from pytsa.preprocessing import exponentially_weighted_moving_average
    return {
        "timeseries":exponentially_weighted_moving_average(get_single_input(input_collection),args.get("span",25)),
        "sources":input_collection,
        "process_id":"EMWA",
    }

def peak_signals(input_collection:List[pd.Series], args:Dict) -> Dict:
    from pytsa.preprocessing import peakdetect
    peaks = peakdetect(get_single_input(input_collection))
    return {
        "timeseries": peaks[0].data,
        "max": peaks[1].data,
        "sources":input_collection,
        "process_id":"peak_signals",
    }

def seasonal_decomposition(input_collection:List[pd.Series], args:Dict) -> Dict:
    from pytsa.trend import seasonal_decomposition
    result = seasonal_decomposition(get_single_input(input_collection),model="additive",freq=10)
    seasonal = result.seasonal
    return {
        "timeseries":seasonal,
        "trend": result.trend,
        "residual": result.resid,
        "sources":input_collection,
        "process_id":"seasonal_decomposition",
    }

def fourier_extrapolation(input_collection:List[pd.Series], args:Dict) -> Dict:
    from pytsa.preprocessing import fourierExtrapolation
    result = fourierExtrapolation(get_single_input(input_collection),5)
    seasonal = result
    return {
        "timeseries":seasonal,
        "sources":input_collection,
        "process_id":"fourier_extrapolation",
    }

def rolling_mean(input_collection:List[pd.Series], args:Dict) -> Dict:
    from pytsa.preprocessing import rolling_mean
    result = rolling_mean(get_single_input(input_collection),5)
    seasonal = result
    return {
        "timeseries":seasonal,
        "sources":input_collection,
        "process_id":"rolling_mean",
    }

def savitzky_golay(input_collection:List[pd.Series], args:Dict):
    from pytsa.preprocessing import savitz_golay
    result = savitz_golay(get_single_input(input_collection))
    seasonal = result
    return {
        "timeseries": seasonal,
        "sources": input_collection,
        "process_id": "savitzky_golay",
    }

def arima_model(input_collection:List[pd.Series], args:Dict):
    from pytsa.advanced import arima_model
    result = arima_model(get_single_input(input_collection))
    seasonal = result
    rss= result[0]
    log_series_diff = result[1]
    results_ARIMA = result[2]
    return {
        "timeseries": results_ARIMA.fittedvalues,
        "log_series_diff": log_series_diff,
        "RSS": rss,
        "sources": input_collection,
        "process_id": "arima_model",
    }


def getProcessImageCollection( process_id:str, args:Dict, viewingParameters):
    collections = extract_arg(args,'timeseries')
    child_collections = list(map(lambda c:graphToRdd(c,viewingParameters),collections))

    print(globals().keys())
    process_function = globals()[process_id]
    if process_function is None:
        raise RuntimeError("No process found with name: "+process_id)
    return process_function(child_collections,args)
