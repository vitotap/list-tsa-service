import sys
import logging

logging.basicConfig(stream=sys.stdout, level=logging.WARN)

from flask import Flask
app = Flask(__name__)
app.config['APPLICATION_ROOT'] = '/tsa'
from flask import json
import pandas as pd

class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, pd.Series):
            return json.loads(obj.to_json(date_format="iso"))
        if isinstance(obj, pd.DataFrame):
            return json.loads(obj.to_json(date_format="iso"))
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

app.json_encoder = ComplexEncoder
import pytsa_service.views